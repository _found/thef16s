$(function() {
	// $('#fullpage').fullpage({
	// 	menu: '#navbar-main'
	// });

	$('.sm-menu').on('click', function() {
		$('nav .sm-nav-menu').show();
	});

	$('.sm-menu-close').on('click', function() {
		$('nav .sm-nav-menu').hide();
	});

	$('.sm-nav-menu a').on('click', function() {
		$('nav .sm-nav-menu').hide();
	});

	function sizeHandler() {
		if ($(window).width()  > 991 ) {
			$('nav .sm-nav-menu').hide();
		}
	}

	sizeHandler();

	$(window).resize(function(){
		sizeHandler();
	});

	function scrollToSection(target) {
		var offset = 60;
		if ($(window).width()  > 991 ) {
			offset = ($('#navbar-main').offset().top);
		}

		$('html,body').animate({
			scrollTop: $(target).offset().top - offset
		}, 1000);
	}

	$('.intra-link').on('click', function() {
		scrollToSection($(this).attr('href'));
	});

	$('#navbar-main a').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					scrollToSection(target);
					return false;
				} 
		}
	});

});